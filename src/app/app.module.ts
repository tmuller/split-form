import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LibraryItemDetailComponent } from './components/library-item-detail/library-item-detail.component';
import { LibraryItemListComponent } from './components/library-item-list/library-item-list.component';
import { BookDetailComponent } from './components/library-item-detail/book-detail/book-detail.component';
import { CdDetailComponent } from './components/library-item-detail/cd-detail/cd-detail.component';
import { DvdDetailComponent } from './components/library-item-detail/dvd-detail/dvd-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    LibraryItemDetailComponent,
    LibraryItemListComponent,
    BookDetailComponent,
    CdDetailComponent,
    DvdDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    CdDetailComponent,
    DvdDetailComponent,
    BookDetailComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
