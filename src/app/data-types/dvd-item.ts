export class DvdItem {
  lengthInSeconds: number;
  tracks: string[];
  isBlueRay = true;
}
