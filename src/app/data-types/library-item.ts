export class LibraryItem<T> {
  id?: number;
  type: string;
  artistOrAuthor: string;
  title: string;
  yearReleased: number;
  deweyIdx: string;
  details: T;
}
