import { BookItem } from './book-item';
import { CdItem } from './cd-item';
import { DvdItem } from './dvd-item';
import { LibraryItem } from './library-item';

export type LibraryTypeUnion = LibraryItem<BookItem> | LibraryItem<CdItem> | LibraryItem<DvdItem>;
