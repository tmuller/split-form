import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryItemDetailComponent } from './components/library-item-detail/library-item-detail.component';
import { LibraryItemListComponent } from './components/library-item-list/library-item-list.component';


const routes: Routes = [
  {
    path: 'list',
    component: LibraryItemListComponent
  },
  {
    path: 'list/new',
    component: LibraryItemDetailComponent,
    data: {createNew: true}
  },
  {
    path: 'list/:id',
    component: LibraryItemDetailComponent,
    data: {createNew: false}
  },
  {
    path: '**',
    redirectTo: '/list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
