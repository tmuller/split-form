import { LibraryTypeUnion } from '../data-types/library-item-union';

export const libraryItemList: LibraryTypeUnion[] = [
  {
    id: 1,
    type: 'book',
    artistOrAuthor: 'Arthur Cunningham',
    title: 'Secret lives of bees',
    yearReleased: 1956,
    deweyIdx: 'M45 345.42 T 34',
    details: {
      numPages: 256,
      edition: 2
    }
  },
  {
    id: 2,
    type: 'cd',
    artistOrAuthor: 'Dream Theater',
    title: 'Scenes From a Memory',
    yearReleased: 1999,
    deweyIdx: 'D143 970.42 T 34.4',
    details: {
      lengthInSeconds: 4534,
      songs: ['Overture', 'Run for it']
    }
  },
  {
    id: 3,
    type: 'dvd',
    artistOrAuthor: '',
    title: 'Friends',
    yearReleased: 1996,
    deweyIdx: 'M45 345.42 T 34',
    details: {
      lengthInSeconds: 7495,
      tracks: ['one', 'two'],
      isBlueRay: true
    }
  },
  {
    id: 4,
    type: 'cd',
    artistOrAuthor: 'Pain Of Salvation',
    title: 'The Perfect Element',
    yearReleased: 2000,
    deweyIdx: 'C056 0563.65 R 98.1',
    details: {
      lengthInSeconds: 4634,
      songs: ['Used', 'In The Flesh']
    }
  },
];
