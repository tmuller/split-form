import { Component } from '@angular/core';
import { LibraryTypeUnion } from '../../data-types/library-item-union';
import { libraryItemList } from '../../data/sample-data';

@Component({
  selector: 'app-library-item-list',
  templateUrl: './library-item-list.component.html',
})
export class LibraryItemListComponent<LibraryTypes> {

  private libraryItemList: LibraryTypeUnion[];

  constructor() {
    this.libraryItemList = libraryItemList;
  }



}
