import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AbstractLibItemDetail } from '../abstract-lib-item-detail/abstract-lib-item-detail';

@Component({
  selector: 'app-cd-detail',
  templateUrl: './cd-detail.component.html',
})
export class CdDetailComponent extends AbstractLibItemDetail {

  constructor(public fb: FormBuilder) {
    super();
    this.childForm = this.fb.group({
      lengthInSeconds: [''],
      songs: ['']
    });
  }
}
