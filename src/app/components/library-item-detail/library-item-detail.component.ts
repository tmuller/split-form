import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LibraryTypeUnion } from '../../data-types/library-item-union';
import { libraryItemList } from '../../data/sample-data';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { CdDetailComponent } from './cd-detail/cd-detail.component';
import { DvdDetailComponent } from './dvd-detail/dvd-detail.component';

@Component({
  selector: 'app-library-item-detail',
  templateUrl: './library-item-detail.component.html',
})
export class LibraryItemDetailComponent implements OnInit {

  public formDefinition: FormGroup;

  // set by the route snapshot via a parameter defined in routing module
  public createNewLibraryItem: boolean;

  /**
   * The sequence in this object determines the default setting in the UI,
   * as the first item is the default for an empty form
   */
  private detailContentStyles = {
    book: {label: 'Book', component: BookDetailComponent},
    cd: {label: 'CD', component: CdDetailComponent},
    dvd: {label: 'DVD', component: DvdDetailComponent},
  };

  private iterableContentTypes = [];

  private childComponent: any;

  @ViewChild('itemDetail', {read: ViewContainerRef, static: true})
  itemDetailContainer: ViewContainerRef;

  constructor(
    public fb: FormBuilder,
    public route: ActivatedRoute,
    private componentFactoryResolver: ComponentFactoryResolver,
  ) {

    this.formDefinition = this.fb.group({
      id: [''],
      type: [''],
      artistOrAuthor: [''],
      title: [''],
      yearReleased: [''],
      deweyIdx: [''],
    });
  }

  ngOnInit() {
    this.makeItemOptionIterable();
    this.createNewLibraryItem = this.route.snapshot.data.createNew;
    const itemId = this.route.snapshot.paramMap.get('id');
    let formData: LibraryTypeUnion = this.formDefinition.value;

    // temporary way to get an entry from hard-coded list. Later in
    if (itemId) {
      const libraryItemId = parseInt(itemId, 10);
      formData = libraryItemList.filter(this.filterListById(libraryItemId))[0];
    }  else formData.type = this.iterableContentTypes[0].id;

    this.instantiateDetailForm(formData);
  }

  private makeItemOptionIterable() {
    for (const option in this.detailContentStyles) {
      if (this.detailContentStyles.hasOwnProperty(option)) {
        this.iterableContentTypes.push({id: option, label: this.detailContentStyles[option].label});
      }
    }
  }

  private filterListById(id: number): (libraryItem: LibraryTypeUnion) => boolean {
    return (libraryItem: LibraryTypeUnion): boolean => {
      return libraryItem.id === id;
    };
  }

  public onFormSubmit() {
    console.log('submitting', this.formDefinition.value);
  }

  private instantiateDetailForm(formData: LibraryTypeUnion) {
    const childComponentRef = this.detailContentStyles[formData.type].component;
    const factoryInstance = this.componentFactoryResolver.resolveComponentFactory(childComponentRef);
    this.childComponent = this.itemDetailContainer.createComponent(factoryInstance);

    this.rebuildFormAndPopulateWith(formData);
  }

  private rebuildFormAndPopulateWith(formData: LibraryTypeUnion): void {
    const instance = this.childComponent.instance;
    this.formDefinition.removeControl('details');
    this.formDefinition.addControl('details', instance.childForm as FormGroup);
    this.formDefinition.patchValue(formData);
  }

  public onMediaTypeSelectionChanged($event) {
    this.itemDetailContainer.clear();
    this.instantiateDetailForm(this.formDefinition.value);
  }

}
