import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AbstractLibItemDetail } from '../abstract-lib-item-detail/abstract-lib-item-detail';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
})
export class BookDetailComponent extends AbstractLibItemDetail {

  constructor(public fb: FormBuilder) {
    super();
    this.childForm = this.fb.group({
      numPages: [''],
      edition: [''],
    });
  }
}
