import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AbstractLibItemDetail } from '../abstract-lib-item-detail/abstract-lib-item-detail';

@Component({
  selector: 'app-dvd-detail',
  templateUrl: './dvd-detail.component.html',
})
export class DvdDetailComponent extends AbstractLibItemDetail {

  constructor(public fb: FormBuilder) {
    super();
    this.childForm = this.fb.group({
      lengthInSeconds: [''],
      tracks: [''],
      isBlueRay: [''],
    });
  }

}
